package com.appengine.cuteCatsAppEngine.model;

/**
 * Created by Platon George on 30.09.2014.
 */

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import java.util.Date;

/** The Objectify object model for device registrations we are persisting */
@Entity
public class Cat {

    @Id
    String id;

    @Index
    String name;

    @Index              //each field that has to be queried, needs an index on it!
    String ownerName;

    Date bornDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public Date getBornDate() {
        return bornDate;
    }

    public void setBornDate(Date bornDate) {
        this.bornDate = bornDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
