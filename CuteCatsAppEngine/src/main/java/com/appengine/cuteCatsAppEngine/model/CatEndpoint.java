package com.appengine.cuteCatsAppEngine.model;

import com.appengine.cuteCatsAppEngine.RegistrationRecord;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;

import java.util.List;
import java.util.logging.Logger;

import javax.inject.Named;

import static com.appengine.cuteCatsAppEngine.OfyService.ofy;

/** An endpoint class we are exposing */
@Api(name = "catEndpoint", version = "v1", namespace = @ApiNamespace(ownerDomain = "model.cuteCatsAppEngine.appengine.com", ownerName = "model.cuteCatsAppEngine.appengine.com", packagePath=""))
public class CatEndpoint {

    // Make sure to add this endpoint to your web.xml file if this is a web application.

    private static final Logger LOG = Logger.getLogger(CatEndpoint.class.getName());

    @ApiMethod(name = "listCats")
    public CollectionResponse<Cat> listCats(@Named("count") int count) {
        List<Cat> records = ofy().load().type(Cat.class).limit(count).list();
        return CollectionResponse.<Cat>builder().setItems(records).build();
    }


    /**
     * This inserts a new <code>Cat</code> object.
     * @param cat The object to be added.
     * @return The objec2t to be added.
     */
    @ApiMethod(name = "insertCat")
    public Cat insertCat(Cat cat) {
        LOG.info("Calling insertCat method");
        ofy().save().entity(cat).now();
        return cat;
    }
}