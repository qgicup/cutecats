package com.appengine.cutecats.tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.appengine.cutecats.CloudEndpointUtils;
import com.appengine.cutecatsappengine.model.catEndpoint.CatEndpoint;
import com.appengine.cutecatsappengine.model.catEndpoint.model.Cat;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.util.DateTime;

import java.io.IOException;
import java.util.Date;

/**
 * Created by Platon George on 30.09.2014.
 */
public class CatEndpointTask extends AsyncTask<Object, Integer, Long> {

    public static final int TASK_INSERT = 0;
    public static final int TASK_LIST = 1;
    public static final int TASK_DELETE = 2;
    public static final int TASK_GET = 3;


    public static final String TAG = "CatEndpointTask";

    private int choosenAction;

    private ICatEndpointTask mCallback;

    public CatEndpointTask() {
    }

    public CatEndpointTask(int choosenActionArg) {
        choosenAction = choosenActionArg;
    }

    public CatEndpointTask(int choosenActionArg, ICatEndpointTask mCallbackArg) {
        choosenAction = choosenActionArg;
        mCallback = mCallbackArg;
    }

    protected Long doInBackground(Object... contexts) {
        switch (choosenAction) {
            case TASK_INSERT:
                if (contexts.length < 1) {
                    throw new IllegalArgumentException("Please provide the cat instance!");
                }
                insertCat((Cat) contexts[0], mCallback);
                break;
            case TASK_LIST:
                listCats(mCallback);
                break;
        }
        return (long) 0;
    }

    private static CatEndpoint.Builder createEndPointBuilder() {
        CatEndpoint.Builder builder = new CatEndpoint.Builder(AndroidHttp.newCompatibleTransport(), new AndroidJsonFactory(), null);
        return CloudEndpointUtils.updateBuilder(builder);
    }

    /**
     * It could go either asynchronous, either on the same thread
     */
    public static void insertCat(Cat cat, ICatEndpointTask iCatEndpointTask) {
        CatEndpoint endpoint = createEndPointBuilder().build();
        try {
            Date now = new Date();
            cat.setId(new DateTime(now).toString());
            Cat insertedCat = endpoint.insertCat(cat).execute();
            iCatEndpointTask.onCatInserted(insertedCat);
            Log.d(TAG, "Inserted cat" + cat.toString());
        } catch (IOException e) {
            Log.e(TAG, "Cannot insert cat " + cat.toString() + " due :" + e.getMessage());
        }
    }

    /**
     * It could go either asynchronous, either on the same thread
     */
    public static void listCats(ICatEndpointTask iTaskLocationPingCallback) {
        CatEndpoint endpoint = createEndPointBuilder().build();
        try {
            iTaskLocationPingCallback.onCatListRetrieved(endpoint.listCats(100).execute().getItems());
        } catch (IOException e) {
            Log.e(TAG, "Cannot retrieve cats due :" + e.getMessage());
        }
    }
}
