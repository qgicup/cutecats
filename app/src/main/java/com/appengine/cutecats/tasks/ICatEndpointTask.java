package com.appengine.cutecats.tasks;

import com.appengine.cutecatsappengine.model.catEndpoint.model.Cat;

import java.util.List;

/**
 * Created by Platon George on 30.09.2014.
 */
public interface ICatEndpointTask {
    public void onCatListRetrieved(List<Cat> cats);
    public void onCatInserted(Cat cat);
}
