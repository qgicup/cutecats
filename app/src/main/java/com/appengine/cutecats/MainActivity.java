package com.appengine.cutecats;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;


import com.appengine.cutecats.tasks.CatEndpointTask;
import com.appengine.cutecats.tasks.ICatEndpointTask;
import com.appengine.cutecatsappengine.model.catEndpoint.model.Cat;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity {

    ListView mListViewCats;
    EditText mEditTextCatName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initGui();
        initializeListView();
    }

    /**
     * Will fetch existing data
     */
    private void initializeListView() {
        new CatEndpointTask(CatEndpointTask.TASK_LIST, new ICatEndpointTask() {
            @Override
            public void onCatListRetrieved(List<Cat> cats) {
                updateListWithNewData(cats);
            }

            @Override
            public void onCatInserted(Cat cat) { }
        }).execute();
    }

    private void initGui() {
        mListViewCats = (ListView) findViewById(R.id.listview_cats);
        mEditTextCatName = (EditText) findViewById(R.id.editText_catname);
        findViewById(R.id.button_addCat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String catName = mEditTextCatName.getText().toString();
                insertCatAndUpdateList(catName);
            }
        });
    }

    private void insertCatAndUpdateList(String catName){
        if(catName != null && !catName.isEmpty()) {
            new CatEndpointTask(CatEndpointTask.TASK_INSERT, new ICatEndpointTask() {
                @Override
                public void onCatListRetrieved(List<Cat> locationPingList) {}

                @Override
                public void onCatInserted(Cat cat) {
                    //we are still on the background thread here, the results were not yet published into the main thread ;)
                    CatEndpointTask.listCats(new ICatEndpointTask() {
                        @Override
                        public void onCatListRetrieved(final List<Cat> catsList) {
                            MainActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    updateListWithNewData(catsList);
                                }
                            });
                        }

                        @Override
                        public void onCatInserted(Cat cat) {}
                    });
                }
            }).execute(ModelBuilder.createCat(catName, "GeorgieBoy"));
        }
    }


    private void updateListWithNewData(List<Cat> catList) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.simple_list_item, extractCatNames(catList));
        mListViewCats.setAdapter(adapter);
    }

    private static List<String> extractCatNames(List<Cat> catList){
        List<String> catNames = new ArrayList<String>();
        for(Cat cat: catList) {
            catNames.add(cat.getName());
        }
        return catNames;
    }
}
