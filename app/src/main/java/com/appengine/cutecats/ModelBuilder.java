package com.appengine.cutecats;

import com.appengine.cutecatsappengine.model.catEndpoint.model.Cat;
import com.google.api.client.util.DateTime;

import java.util.Date;

/**
 * Created by Platon George on 30.09.2014.
 */
public class ModelBuilder {

    public static Cat createCat(String name, String ownerName) {
        Cat cat = new Cat();
        cat.setName(name);
        cat.setOwnerName(ownerName);
        cat.setBornDate(new DateTime(new Date()));
        return cat;
    }
}
